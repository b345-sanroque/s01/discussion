package com.zuitt.wdc044.services;

import com.zuitt.wdc044.models.User;
import com.zuitt.wdc044.repositories.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Optional;

//to indicate that we are going to implement a Service annotation from spring framework
@Service
public class UserServiceImpl {

    //Dependency Injection
    //@Autowired means that instance of a class that implements
    //When it comes to the userRepository property, it will contain an instance of the UserRepository
    @Autowired
    private UserRepository userRepository;

}
